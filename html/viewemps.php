<html>
<head>
<title>View Employees</title>
</head>

<body>
<?php
  include 'header.php';
  require_once 'credentials.php';

  @ $db = new mysqli('localhost', $mgruser, $mgrpass, 'hotelDB');
      if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit;
      }


  $ssn = $db->real_escape_string($_POST['ssn']);
  $firstlast = $db->real_escape_string($_POST['firstlast']);
  $minAge = $db->real_escape_string($_POST['minAge']);
  $dob1 = date("Y-m-d", mktime(0, 0, 0, date("n"), date("j"), date("Y") - $minAge));
  $maxAge = $db->real_escape_string($_POST['maxAge']);
  $dob2 = date("Y-m-d", mktime(0, 0, 0, date("n"), date("j"), date("Y") - $maxAge));
  $minSal = $db->real_escape_string($_POST['minSal']);
  $maxSal = $db->real_escape_string($_POST['maxSal']);
  $department = ($_POST['department']);
?>

<h1>Search Results</h1>

<?php
    printf("SSN: %s<br />", $ssn);
    printf("Name: %s<br />", $firstlast);
    printf("Age: %s to %s<br />", $minAge, $maxAge);
    printf("DOB: %s to %s<br /", $minDob, $maxDob);
    printf("Salary: %s to %s<br />", $minSal, $maxSal);
    printf("Department: %d", $department);

    $query = "SELECT DISTINCT ssn, dob, name, hiredate, salary FROM Emps NATURAL JOIN WorksIn ";
    $where = "WHERE ";
    $andNeeded = false;
    if (!empty($ssn)) {
      $where .= "ssn=$ssn ";
      $andNeeded = true;
    }
    if (!empty($firstlast)) {
      if ($andNeeded) $where .="and ";
      $where .= "name=$firstlast ";
      $andNeeded = true;
    }
    if (!empty($minAge)) {
      if ($andNeeded) $where .="and ";
      $where .= "dob<=\"$dob1\" ";
      $andNeeded = true;
    }
    if (!empty($maxAge)) {
      if ($andNeeded) $where .="and ";
      $where .= "dob>=\"$dob2\" ";
      $andNeeded = true;
    }
    if (!empty($minSal)) {
      if ($andNeeded) $where .="and ";
      $where .= "salary>=$minSal ";
      $andNeeded = true;
    }
    if (!empty($maxSal)) {
      if ($andNeeded) $where .="and ";
      $where .= "salary<=$maxSal ";
      $andNeeded = true;
    }
    if (!empty($department)) {
      if ($andNeeded) $where .="and ";
      $where .= "did=$department ";
      $andNeeded = true;
    }
    if ($andNeeded) $query .= $where;
    printf("<p>Query: %s</p>", $query);

    $result = $db->query($query);
    $num_results = $result->num_rows;
    /*
    $query = "select * from Departments where hotel=".$hotelid." order by name";
    $result = $db->query($query);
    $num_results = $result->num_rows;
    echo '<td colspan="2"><select name="department">'."\n";
    echo '<option value=-1></option>'."\n";
    for($i = 0; $i < $num_results; $i++) {
      $row = $result->fetch_assoc();
      echo '<option value='.$row['did'].'>'.stripslashes($row['name'])."</option>\n";
    }
    echo "</select></td>\n";
    $result->free();
    $db->close();
    */
?>

<table>
  <tr>
    <th>DOB</th>
    <th>Name</th>
    <th>Hired</th>
    <th>Salary</th>
    <th>SSN</th>
  </tr>
<form action="empdetails.php" method="post">
<?php
  for($i = 0; $i < $num_results; $i++) {
    $row = $result->fetch_assoc();
    echo '<tr>';
    printf("<td>".$row['dob']."</td>");
    printf("<td>".$row['name']."</td>");
    printf("<td>".$row['hiredate']."</td>");
    printf("<td>".$row['salary']."</td>");
    printf('<td><input type="submit" name="targetEmp" value="'.$row['ssn'].'"</td>');
    echo '</tr>';
  }
  $result->free();
  $db->close();
?>
</form>
</table>

</body>

</html>