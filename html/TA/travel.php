<!DOCTYPE HTML>
<html>
    <body>
        <?php 
            include 'header.php';
            require_once('credentials.php');
        ?>
        <h1>Tavel Agents!<h1>
            
            <h2>Please select what you would like to do </h2>
            
                
            <h3>Search Hotels</h3>
            <form action="hotelsearch.php" method="post">
                <p>Name: <input type="text" name="name" maxlength="50"></p>
                <p><input type="submit"></p>
            </form>
                OR
                <br>
            <form action="hotelsearchstars.php" method="post">
                <p>State:
                    <select name="state">
                        <option value="AL">Alabama</option>
                        <option value="AK">Alaska</option>
                        <option value="AZ">Arizona</option>
                        <option value="AR">Arkansas</option>
                        <option value="CA">California</option>
                        <option value="CO">Colorado</option>
                        <option value="CT">Connecticut</option>
                        <option value="DE">Delaware</option>
                        <option value="DC">District Of Columbia</option>
                        <option value="FL">Florida</option>
                        <option value="GA">Georgia</option>
                        <option value="HI">Hawaii</option>
                        <option value="ID">Idaho</option>
                        <option value="IL">Illinois</option>
                        <option value="IN">Indiana</option>
                        <option value="IA">Iowa</option>
                        <option value="KS">Kansas</option>
                        <option value="KY">Kentucky</option>
                        <option value="LA">Louisiana</option>
                        <option value="ME">Maine</option>
                        <option value="MD">Maryland</option>
                        <option value="MA">Massachusetts</option>
                        <option value="MI">Michigan</option>
                        <option value="MN">Minnesota</option>
                        <option value="MS">Mississippi</option>
                        <option value="MO">Missouri</option>
                        <option value="MT">Montana</option>
                        <option value="NE">Nebraska</option>
                        <option value="NV">Nevada</option>
                        <option value="NH">New Hampshire</option>
                        <option value="NJ">New Jersey</option>
                        <option value="NM">New Mexico</option>
                        <option value="NY">New York</option>
                        <option value="NC">North Carolina</option>
                        <option value="ND">North Dakota</option>
                        <option value="OH">Ohio</option>
                        <option value="OK">Oklahoma</option>
                        <option value="OR">Oregon</option>
                        <option value="PA">Pennsylvania</option>
                        <option value="RI">Rhode Island</option>
                        <option value="SC">South Carolina</option>
                        <option value="SD">South Dakota</option>
                        <option value="TN">Tennessee</option>
                        <option value="TX">Texas</option>
                        <option value="UT">Utah</option>
                        <option value="VT">Vermont</option>
                        <option value="VA">Virginia</option>
                        <option value="WA">Washington</option>
                        <option value="WV">West Virginia</option>
                        <option value="WI">Wisconsin</option>
                        <option value="WY">Wyoming</option>
                    </select>               
                    <br>

                    Stars:
                    <select name="starsMin">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                    to
                    <select name="starsMax">
                        <option value="5">5</option>
                        <option value="4">4</option>
                        <option value="3">3</option>
                        <option value="2">2</option>
                        <option value="1">1</option>
                    </select>
                </p>
                <p>
                    <input type="submit" name="formSubmit" value="Submit">
                </p>
            </form>
            
            <h3>Reviews:</h3>
            <form action="readreview.php" method="post">
                <input type="submit" name="formSubmit" value="Read Reviews">
            </form>
            
            <h3>Modify Bookings</h3>
            <form action="makebooking.php" method="post">
                <input type="submit" name="formSubmit" value="New Booking">
            </form>
            
            <form action="modifybooking.php" method="post">
                <input type="submit" name="formSubmit" value="Modify Booking">
            </form>
            
    </body>
</html>