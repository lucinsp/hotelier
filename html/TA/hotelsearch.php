<html>
    <body>
<?php 
    include 'header.php';
    require_once('credentials.php');
?>
Welcome 


<?php
$search = strtolower($_POST["name"]);
echo $search;

// Create connection
$conn = new mysqli("localhost", $username, $password, "hotelDB");
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = " SELECT H.name, H.stars, L.street, L.city, L.state, L.zip
         FROM Hotels H, Location L
         WHERE LOWER(H.name) LIKE '%$search%' AND H.location=L.id";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    echo "<table border=1>";
    echo "<tr>";
    echo "<th>Name</th>";
    echo "<th>Stars</th>";
    echo "<th>Address</th>";
    echo "</tr>";
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "<tr>";
        echo "<td>" . $row["name"]. "</td>";
        echo "<td>" . $row["stars"]. "</td>";
        echo "<td>" . $row["street"]." ". $row["city"].", ". $row["state"]." ". $row["zip"]. "</td>";
        echo "</tr>";
    }
    echo "</table>";
} else {
    echo "0 results";
}
$conn->close();
?>

    </body>
</html>