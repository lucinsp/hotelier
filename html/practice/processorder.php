<html>

<head>
<title>Bob's Auto Parts - Order Results</title>
</head>

<body>
<h1>Bob's Auto Parts</h1>
<h2>Order Results</h2>

<?php

ini_set('display_errors', 'On');
error_reporting(E_ALL);

$ip = $_SERVER['REMOTE_ADDR'];
echo "IP Address: $ip</br>";

phpinfo();

$user = `whoami`;
echo '<p>PHP running as '.$user.'.</p>';

echo "<p>This is the first line."
	."This is the second line.</p>";

// create short variable names
$tireqty = $_POST['tireqty'];
$oilqty = $_POST['oilqty'];
$sparkqty = $_POST['sparkqty'];
$find = $_POST['find'];

date_default_timezone_set("America/New_York");
echo '<p>Order processed at ';
echo date('H:i, jS F Y');
echo '<p>';
echo '<p>Your order is as follows: </p>';
echo $tireqty.' tires<br />';
echo $oilqty.' bottles of oil<br />';
echo $sparkqty.' spark plugs<br />';

define('TIREPRICE', 100);
define('OILPRICE', 10);
define('SPARKPRICE', 4);

$totalqty = $tireqty + $oilqty + $sparkqty;
echo "<p>Items ordered: $totalqty.<br />";

$totalamount = $tireqty * TIREPRICE
    + $oilqty * OILPRICE
    + $sparkqty * SPARKPRICE;
echo "Subtotal: $".number_format($totalamount,2)."<br />";

$taxrate = 0.10; // local tax is 10%
$totalamount *= (1 + $taxrate);
echo 'Total including tax: $'.number_format($totalamount, 2).'</p>';

echo '<p>';
switch($find) {
    case "a" :
        echo 'Regular customer.';
        break;
    case "b" :
        echo 'Customer referred by TV advert.';
        break;
    case "c" :
        echo 'Customer referred by phone directory.';
        break;
    case "d" :
        echo 'Customer referred by word of mouth.';
        break;
    default:
        echo 'We do not know how this customer found us.';
        break;
}
echo '</p>';

?>
</body>

</html>
