<?php
include 'header.php';
?>

<!DOCTYPE HTML> 
<html>
<head>
<style>
.error {color: #FF0230;}
</style>
</head>
<body> 


<h1>New Reservation</h1>
<h3>Personal Information</h3>

<?php
unset($name);
unset($monthf);
unset($dayf);
unset($year);
unset($email);
unset($salary);
unset($street);
unset($city);
unset($state);
unset($zip);
unset($month1f);
unset($day1f);
unset($year1);
unset($month2f);
unset($day2f);
unset($year2);

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  
  if (empty($_POST["name"])) {
     $nameErr = "Name is required";
   } else {
     $name = test_input($_POST["name"]);
     // check if name only contains letters and whitespace
     if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
       $nameErr = "Only letters and white space allowed"; 
     }
   }
 if (empty($_POST["email"])) {
     //$emailErr = "Email is required";
   } else {
     $email = test_input($_POST["email"]);
     // check if e-mail address is well-formed
     if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
       $emailErr = "Invalid email format"; 
     }
   }

     if (empty($_POST["salary"])) {
     //$salaryErr = "Salary is required";
   } else {
     $salary = test_input($_POST["salary"]);
     // check if name only contains letters and whitespace
     //if (!preg_match("/^[0-9]*$/",$salary)) 
        if(!(is_numeric($salary))) //if not numeric = error
        {
             //echo "error";
             $salaryErr = "Only numbers allowed"; 
        }
      
     //}
   }

}
function test_input($data) {
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}
?>

<?php
    $month = date("n"); //without leading zero(o)
    $year = date("Y"); //four digit format
    $st_year = "1935"; //Starting Year
    $month_names = array("January", "February", "March","April", "May", "June", "July", "August", "September", "October", "November", "December");
?>


<form name="Month_Year" id="Month_Year" action="<?php $SITE['REQURI'] ?>" method="post">

<p>
Name: <input type="text" name="name" value="<?php echo $name;?>">
<span class="error">* <?php echo $nameErr;?></span>
</p>

Date of Birth:
<select name="month" id="month">
    <option value = "">Month..</option>
<?php
for ($i=1; $i<=12; $i++) {
    echo "<option ";
    /*if ($i+1 == "January") {
        echo "selected=\"selected\" ";
    }*/
    echo "value=\"$i\">", $month_names[$i-1], "</option>\n";
}
?>
</select>

<select name="day" id="day">
<option value = "">Day..</option>
<?php
for ($i=1; $i<=31; $i++) {
    echo "<option ";

    /*if ($i == $year) {
        echo "selected=\"selected\" ";
    }*/
    echo "value=\"$i\">$i</option>\n";
}
?>
</select>

<select name="year" id="year">
<option value = "">Year..</option>
<?php
for ($i=$st_year; $i<=$year; $i++) {
    echo "<option ";
    /*if ($i+1 == "Select.") {
        echo "selected=\"selected\" ";
    }*/
    echo "value=\"$i\">$i</option>\n";
}
?>
</select>

<br><br>
E-mail: <input type="text" name="email" value="<?php echo $email;?>">
   <span class="error"> <?php echo $emailErr;?></span>
<br><br>

Salary: <input type="text" name="salary" value="<?php echo $salary;?>">
   <span class="error"> <?php echo $salaryErr;?></span>
   <br> <br>

<!-- Street Number: <input type="text" name="streetnum" value="<?php echo $streetnum;?>"> -->

Street: <input type="text" name="street" value="<?php echo $street;?>">
<br>
City: <input type="text" name="city" value="<?php echo $city;?>">
<br>
State: 
<select name="state" id="state">
<option value = "">Select State..</option>
    <option value="AL" <?PHP if($state=="AL") echo "selected";?>>Alabama</option>
    <option value="AK" <?PHP if($state=="AK") echo "selected";?>>Alaska</option>
    <option value="AZ" <?PHP if($state=="AZ") echo "selected";?>>Arizona</option>
    <option value="AR" <?PHP if($state=="AR") echo "selected";?>>Arkansas</option>
    <option value="CA" <?PHP if($state=="CA") echo "selected";?>>California</option>
    <option value="CO" <?PHP if($state=="CO") echo "selected";?>>Colorado</option>
    <option value="CT" <?PHP if($state=="CT") echo "selected";?>>Connecticut</option>
    <option value="DE" <?PHP if($state=="DE") echo "selected";?>>Delaware</option>
    <option value="DC" <?PHP if($state=="DC") echo "selected";?>>District of Columbia</option>
    <option value="FL" <?PHP if($state=="FL") echo "selected";?>>Florida</option>
    <option value="GA" <?PHP if($state=="GA") echo "selected";?>>Georgia</option>
    <option value="HI" <?PHP if($state=="HI") echo "selected";?>>Hawaii</option>
    <option value="ID" <?PHP if($state=="ID") echo "selected";?>>Idaho</option>
    <option value="IL" <?PHP if($state=="IL") echo "selected";?>>Illinois</option>
    <option value="IN" <?PHP if($state=="IN") echo "selected";?>>Indiana</option>
    <option value="IA" <?PHP if($state=="IA") echo "selected";?>>Iowa</option>
    <option value="KS" <?PHP if($state=="KS") echo "selected";?>>Kansas</option>
    <option value="KY" <?PHP if($state=="KY") echo "selected";?>>Kentucky</option>
    <option value="LA" <?PHP if($state=="LA") echo "selected";?>>Louisiana</option>
    <option value="ME" <?PHP if($state=="ME") echo "selected";?>>Maine</option>
    <option value="MD" <?PHP if($state=="MD") echo "selected";?>>Maryland</option>
    <option value="MA" <?PHP if($state=="MA") echo "selected";?>>Massachusetts</option>
    <option value="MI" <?PHP if($state=="MI") echo "selected";?>>Michigan</option>
    <option value="MN" <?PHP if($state=="MN") echo "selected";?>>Minnesota</option>
    <option value="MS" <?PHP if($state=="MS") echo "selected";?>>Mississippi</option>
    <option value="MO" <?PHP if($state=="MO") echo "selected";?>>Missouri</option>
    <option value="MT" <?PHP if($state=="MT") echo "selected";?>>Montana</option>
    <option value="NE" <?PHP if($state=="NE") echo "selected";?>>Nebraska</option>
    <option value="NV" <?PHP if($state=="NV") echo "selected";?>>Nevada</option>
    <option value="NH" <?PHP if($state=="NH") echo "selected";?>>New Hampshire</option>
    <option value="NJ" <?PHP if($state=="NJ") echo "selected";?>>New Jersey</option>
    <option value="NM" <?PHP if($state=="NM") echo "selected";?>>New Mexico</option>
    <option value="NY" <?PHP if($state=="NY") echo "selected";?>>New York</option>
    <option value="NC" <?PHP if($state=="NC") echo "selected";?>>North Carolina</option>
    <option value="ND" <?PHP if($state=="ND") echo "selected";?>>North Dakota</option>
    <option value="OH" <?PHP if($state=="OH") echo "selected";?>>Ohio</option>
    <option value="OK" <?PHP if($state=="OK") echo "selected";?>>Oklahoma</option>
    <option value="OR" <?PHP if($state=="OR") echo "selected";?>>Oregon</option>
    <option value="PA" <?PHP if($state=="PA") echo "selected";?>>Pennsylvania</option>
    <option value="RI" <?PHP if($state=="RI") echo "selected";?>>Rhode Island</option>
    <option value="SC" <?PHP if($state=="SC") echo "selected";?>>South Carolina</option>
    <option value="SD" <?PHP if($state=="SD") echo "selected";?>>South Dakota</option>
    <option value="TN" <?PHP if($state=="TN") echo "selected";?>>Tennessee</option>
    <option value="TX" <?PHP if($state=="TX") echo "selected";?>>Texas</option>
    <option value="UT" <?PHP if($state=="UT") echo "selected";?>>Utah</option>
    <option value="VT" <?PHP if($state=="VT") echo "selected";?>>Vermont</option>
    <option value="VA" <?PHP if($state=="VA") echo "selected";?>>Virginia</option>
    <option value="WA" <?PHP if($state=="WA") echo "selected";?>>Washington</option>
    <option value="WV" <?PHP if($state=="WV") echo "selected";?>>West Virginia</option>
    <option value="WI" <?PHP if($state=="WI") echo "selected";?>>Wisconsin</option>
    <option value="WY" <?PHP if($state=="WY") echo "selected";?>>Wyoming</option>

</select>
<br>
Zipcode: <input type="text" name="zip" value="<?php echo $zip;?>">
<br><br><br>

<h3>Reservation Information: </h3>
FROM:
<select name="month1" id="month1">
    <option value = "">Month..</option>
<?php
for ($i=1; $i<=12; $i++) {
    echo "<option ";
    /*if ($i+1 == "January") {
        echo "selected=\"selected\" ";
    }*/
    echo "value=\"$i\">", $month_names[$i-1], "</option>\n";
}
?>
</select>

<select name="day1" id="day1">
<option value = "">Day..</option>
<?php
for ($i=1; $i<=31; $i++) {
    echo "<option ";

    /*if ($i == $year) {
        echo "selected=\"selected\" ";
    }*/
    echo "value=\"$i\">$i</option>\n";
}
?>
</select>

<select name="year1" id="year1">
<option value = "">Year..</option>
<?php
for ($i=2015; $i<=2017; $i++) {
    echo "<option ";
    /*if ($i+1 == "Select.") {
        echo "selected=\"selected\" ";
    }*/
    echo "value=\"$i\">$i</option>\n";
}
?>
</select>

<br><br>
TO:
<select name="month2" id="month2">
    <option value = "">Month..</option>
<?php
for ($i=1; $i<=12; $i++) {
    echo "<option ";
    /*if ($i+1 == "January") {
        echo "selected=\"selected\" ";
    }*/
    echo "value=\"$i\">", $month_names[$i-1], "</option>\n";
}
?>
</select>

<select name="day2" id="day2">
<option value = "">Day..</option>
<?php
for ($i=1; $i<=31; $i++) {
    echo "<option ";

    /*if ($i == $year) {
        echo "selected=\"selected\" ";
    }*/
    echo "value=\"$i\">$i</option>\n";
}
?>
</select>

<select name="year2" id="year2">
<option value = "">Year..</option>
<?php
for ($i=2015; $i<=2017; $i++) {
    echo "<option ";
    /*if ($i+1 == "Select.") {
        echo "selected=\"selected\" ";
    }*/
    echo "value=\"$i\">$i</option>\n";
}
?>
</select>
<br><br>
Number of Guests: 
<select name="numguests" id="numguests">
<option value = "">Select..</option>
<option>1</option>;
<option>2</option>;
<option>3</option>;
<option>4</option>;
</select>

<br><br>
Payment Method: 
<select name="paymethod" id="paymethod">
<option value = "">Select..</option>
<option>Cash</option>;
<option>Credit/Debit</option>;
<option>Check</option>;
</select>

<br><br>


<h3> Select Room Type </h3>
<?php
$servername = "localhost";
    $username = "csuser";
    $password = "cse4a4ad";
    $dbname = "hotelDB";
    //unset($sfood);
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
         die("Connection failed: " . $conn->connect_error);
    } 

    $sql = "SELECT name, capacity FROM RoomType";
    $result = $conn->query($sql);
    echo "<table border='1'> <tr><th>Room Type</th><th>Capacity</th></tr><br>";
         // output data of each row
    while($row = $result->fetch_assoc()) 
    {
          // echo "<br> Name: ". $row["name"]. " <br>- Price: ". $row["price"]. "<br>";
         echo "<tr>";
         echo '<td>' . $row['name'] . '</td>';
         echo '<td>' . $row['capacity'] . '</td>';
         echo "</tr>\n";
     }
    echo "</table>";
    echo "<br?>** Please select room type based on  how many guests will be staying with you<br>";
    $conn->close();

?>
<!-- <form method="post"> -->
<?php
$servername = "localhost";
$username = "csuser";
$password = "cse4a4ad";
$dbname = "hotelDB";    

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
     die("Connection failed: " . $conn->connect_error);
} 
$sql = "SELECT name FROM RoomType";
$result = $conn->query($sql);
$num_results = $result->num_rows;

echo '<select name="type" id="type">';
echo '<option value=" ">Select..</option>';
$i = 1;
while($row = $result->fetch_assoc())
{
    //echo '<option value= "'.$row['name'].'">'.$row['name'].'</option>';
    echo "<option value=\"$i\">".$row['name']."</option>";
    $i++;
}
echo "</select><br>";

$conn->close();
?>
<input type="submit" name="submit" value="Submit"><br>
</form>

<!-- PRINTING -->
<p>
<?php
echo "<h3>INPUT</h3>";

$type = $_POST['type'];
echo $type,"<br>";
$month = $_POST['month'];
$monthf = sprintf("%02d", $month);

$day = $_POST['day'];
$dayf = sprintf("%02d", $day);

$year = $_POST['year'];

$name = $_POST['name'];
$email = $_POST['email'];
$salary = $_POST['salary'];
//$streetnum = $_POST['streetnum'];
$street = $_POST['street'];
$city = $_POST['city'];
$state = $_POST['state'];
$zip = $_POST['zip'];

//FROM
$month1 = $_POST['month1'];
$month1f = sprintf("%02d", $month1);
$day1 = $_POST['day1'];
$day1f = sprintf("%02d", $day1);
$year1 = $_POST['year1'];


//TO
$month2 = $_POST['month2'];
$month2f = sprintf("%02d", $month2);
$day2 = $_POST['day2'];
$day2f = sprintf("%02d", $day2);
$year2 = $_POST['year2'];

$numguests = $_POST['numguests'];

$paymethod = $_POST['paymethod'];

$date =date('$year','$month','$day');
echo "DATE: ", $date, "<br>"; 


echo "Name: " , $name, "<br>"; 
echo "DOB: ", $monthf , " " , $dayf, " ", $year, "<br>";
echo "Email: ", $email, "<br>";
echo "Salary: $", $salary, "<br>";
echo "Address: <br>";
//echo $streetnum, " ";
echo $street, "<br>";
echo $city, ", ";
echo $state, " ";
echo $zip, "<br>";

echo "FROM: ", $month1f , " " , $day1f, " ", $year1, "<br>";
echo "TO: ", $month2f , " " , $day2f, " ", $year2, "<br>";
echo "Numguests: ",$numguests,"<br>";
echo "Pay Method: ", $paymethod, "<br>";
?>
</p>

<?php
unset($_POST);
unset($_REQUEST);

$date1 = $year . '-' . $monthf . '-' . $dayf;
$date2 = $year1 . '-' . $month1f . '-' . $day1f; //FROM
$date3 = $year2 . '-' . $month2f . '-' . $day2f; //TO

$servername = "localhost";
$username = "csuser";
$password = "cse4a4ad";
$dbname = "hotelDB";    

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
     die("Connection failed: " . $conn->connect_error);
} 

//if(isset($_POST["submit"])){

 //to find address & addressID
/*$sql = "INSERT INTO Location(id, street, city, state, zip)
        VALUES (0,'$street','$city', '$state', '$zip' )";
$result = $conn->query($sql);

if ($result === TRUE) {
    echo "New record created successfully<br>";
} 
else {
    echo "Error: " . $sql . "<br>" . $conn->error;
} 
*/
$sql = "SELECT id FROM Location WHERE street = '$street' AND zip='$zip'  ";
$result = $conn->query($sql);

if ($result->num_row > 0) {
    echo "New record created successfully<br>";
} 
else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}

  while($row = $result->fetch_assoc()) {
     //echo "<br><br> AddressID for ", $name, " is ", $row['id'], "<br>";
    $addressID = $row['id'];
    }

//find available room based on selected room type
$sql = "SELECT RoomNum FROM Room R
        WHERE type = $type
        AND NOT EXISTS 
        (
            SELECT roomnum
            FROM Stays S
            WHERE S.roomnum = R.RoomNum 
            AND from1 < $date2 
            AND to1 > $date3 
        )";
        
$result = $conn->query($sql);

if ($result->num_row > 0) {
    echo "New record created successfully<br>";
} 
else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}

  while($row = $result->fetch_assoc()) {
     echo "<br><br> Room numbers for type ", $type, " are ", $row['RoomNum'], "<br>";
     echo $row['roomnum'], "<br>", $row['RoomNum'],"<br>";
   // $addressID = $row['id'];
    }


//since guestID is a key it automatically goes to the next one
/*//need to return that guestID
$sql = "INSERT INTO Guest(guestID, name, dob, salary, email, addressID)
        VALUES (0,'$name','$date1', $salary, '$email', $addressID )";
$result = $conn->query($sql);

if ($result === TRUE) {
    echo "New record created successfully<br>";
} 
else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}
*/
//selects guestID
$sql = "SELECT guestID FROM Guest WHERE name = '$name' AND dob='$date1'  ";
$result = $conn->query($sql);

if ($result->num_row > 0) {
    echo "New record created successfully<br>";
} 
else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}

  while($row = $result->fetch_assoc()) {
     echo "<br><br> GuestID for ", $name, " is ", $row['guestID'], "<br>";
    $returngid = $row['guestID'];
    }



$sql = "INSERT INTO Stays(guestID, reservation, from1, to1, numguests, roomnum,   hotelid, payvalue, paymethod)
        VALUES ($returngid, 0, '$date2', '$date3', $numguests, 0, 1, 0, '$paymethod')";
$result = $conn->query($sql);

if ($result === TRUE) {
    echo "New record created successfully<br>";
} 
else {
    echo "Error: " . $sql . "<br>" . $conn->error;
} 
//}


$conn->close();
?>  


</form>
</body>
</html>