<html>
<head>
<title>Management Interface</title>
<link rel="stylesheet" href="formalize.css" />
<link rel="stylesheet" href="manager.css" />
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="jquery.formalize.js"></script>

<script type="text/javascript">
function validateForm() {
    var x = document.forms["feform"]["minAge"].value;
    var y = document.forms["feform"]["maxAge"].value;
    if (x != "" && y != "" && Number(x) > Number(y)) {
        alert("Minimum age cannot exceed maximum age.");
        return false;
    }
    x = document.forms["feform"]["minSal"].value;
    y = document.forms["feform"]["maxSal"].value;
    if (x != "" && y != "" && Number(x) > Number(y)) {
        alert("Minimum salary cannot exceed maximum salary.");
        return false;
    }
}
</script>
</head>

<body>

<?php
  error_reporting(-1);
  ini_set('display_errors', 'On');
  include 'header.php';
  require_once('credentials.php');
  $hotelid="1";

  @ $db = new mysqli('localhost', $mgruser, $mgrpass, 'hotelDB');
  if (mysqli_connect_errno()) {
    echo '</tr></table><p>Error: Could not connect to database. Please try again later.</p>';
    exit;
  }

  $firee = $_POST['firee'];
  if (!empty($firee)) {
    $sql = "DELETE FROM Emps WHERE ssn=\"$firee\"";
    echo $sql;
    /*
    if ($db->query($sql)) {
      printf("<p>Person $ssn has been terminated.</p>");
    } else {
      printf("<p>Error updating database: %s</p>", $db->error);
    }
    */
  }

?>

<h1>Find Employees</h1>

<form name="feform" action="viewemps.php" method="post" onsubmit="return validateForm()">
<table>
  <tr class = "center">
    <th>Criteria</th>
    <th colspan="2">Values</th>
  </tr>
    <td>SSN</td>
    <td colspan="2"><input name="ssn" type="text"
        pattern="[0-9]{3}-[0-9]{2}-[0-9]{4}" placeholder="123-45-6789"/></td>
  <tr>
    <td>Name</td>
    <td colspan="2"><input name="firstlast" type="text"/>
  </tr>
  <tr>
    <td>Age Range</td>
    <td><input name="minAge" type="number" min="18" max="100"></td>
    <td><input name="maxAge" type="number" min="18" max="100"></td>
  </tr>
  <tr>
    <td>Salary Range</td>
    <td><input name="minSal" type="number" min="0" max="999999"></td>
    <td><input name="maxSal" type="number" min="0" max="999999"></td>
  </tr>
  <tr>
    <td>Works In</td>
    <?php

    $query = "select * from Dept where hotel=".$hotelid." order by name";
    $result = $db->query($query);
    if ($result === false) {
      echo '</tr></table><p>Query failed: '.mysqli_error($db).'</p>';
    }
    $num_results = $result->num_rows;
    echo '<td colspan="2"><select name="department">'."\n";
    echo '<option value="0"></option>'."\n";
    for($i = 0; $i < $num_results; $i++) {
      $row = $result->fetch_assoc();
      echo '<option value='.$row['did'].'>'.stripslashes($row['name'])."</option>\n";
    }
    echo "</select></td>\n";
    $result->free();
    $db->close();
    ?>
  </tr>
  </table>

  <table>
  <tr>
    <td class="buttons">
      <input class="buttons" type="submit" value="Submit">
      <input class="buttons" type="reset" value="Clear">
    </td>
  </tr>
</table>

</form>

</body>
</html>
