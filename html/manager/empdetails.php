<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Employee Details</title>
<link rel="stylesheet" type="text/css" href="hotelier.css">
</head>

<body>

<?php print_r($_POST); ?>

<?php
  include 'header.php';
  require_once 'credentials.php';
  $maxDate = date("Y-m-d", mktime(0, 0, 0, date("n"), date("j"), date("Y") - 18));

  @ $db = new mysqli('localhost', $mgruser, $mgrpass, 'hotelDB');
  if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit;
  }
  $target = $_POST['targetEmp'];

  /*
   * Update Emps table.
   */
  $runupdate = false;
  $updob = $_POST['newdob'];
  $uphired = $_POST['newhired'];
  $upsal = $_POST['newsal'];

  $clause = "SET ";
  if (!empty($updob)) {
    $clause .= "dob=\"$updob\"";
    $runupdate = true;
  }
  if (!empty($uphired)) {
    if ($runupdate) $clause .= ", ";
    $clause .= "hiredate=\"$uphired\"";
    $runupdate = true;
  }
  if (!empty($upsal)) {
    if ($runupdate) $clause .= ", ";
    $clause .= "salary=$upsal";
    $runupdate = true;
  }
  $sql = "UPDATE Emps ".$clause." WHERE ssn=\"$target\"";
  printf("<p>$sql</p>");
  
  if ($runupdate) {
    if ($db->query($sql)) {
      printf("<p>Update successful.</p>");
    } else {
      printf("<p>Error updating record %s.</p>", $db->error);
    }
  }
  
  /*
   * Update Location table.
   */
  $runupdate = false;
  $upstreet = $_POST['newstreet'];
  $upcity = $_POST['newcity'];
  $upstate = $_POST['newstate'];
  $upzip = $_POST['newzip'];

  $clause = "SET ";
  if (!empty($upstreet)) {
    $clause .= "street=\"$upstreet\"";
    $runupdate = true;
  }
  if (!empty($upcity)) {
    if ($runupdate) $clause .= ", ";
    $clause .= "city=\"$upcity\"";
    $runupdate = true;
  }
  if (!empty($upstate)) {
    if ($runupdate) $clause .= ", ";
    $clause .= "state=\"$upstate\"";
    $runupdate = true;
  }
  if (!empty($upzip)) {
    if ($runupdate) $clause .= ", ";
    $clause .= "zip=\"$upzip\"";
    $runupdate = true;
  }
  $sql = "UPDATE Location ".$clause." WHERE id = (SELECT address FROM Emps WHERE ssn = \"$target\")";
  printf("<p>$sql</p>");

  if ($runupdate) {
    if ($db->query($sql)) {
      printf("<p>Update successful.</p>");
    } else {
      printf("<p>Error updating record %s.</p>", $db->error);
    }
  }

  /*
   * Drop Department.
   */
  $updept = $_POST['dropdept'];
  if (!empty($updept)) {
    $sql = "DELETE FROM WorksIn WHERE ssn=\"$target\" AND did=$updept";
    echo $sql;
    if ($db->query($sql)) {
      printf("<p>$target no longer works in department $updept.</p>");
    } else {
      printf("<p>Error updating database: %s</p>", $db->error);
    }
  }

  /*
   * Add Department.
   */
  $updept = $_POST['newdept'];
  if (!empty($updept)) {
    $sql = "INSERT INTO WorksIn (ssn, did) VALUES (\"$target\", $updept)";
    echo $sql;
    if ($db->query($sql)) {
      printf("<p>$target now works in department $updept.</p>");
    } else {
      printf("<p>Error updating database: %s</p>", $db->error);
    }
  }

  /*
   * First SQL Query used to populate page.
   */
  $query = "SELECT * FROM Emps AS E INNER JOIN Location AS L ON (E.address = L.id) "
            ."WHERE E.ssn = \"$target\"";
  echo $query.'<br/>';
  $result = $db->query($query);
  $row = $result->fetch_assoc();

  printf("<h1>%s</h1>", $row['name']);
  printf("<h2>Basics</h2>");
?>

<form action="empdetails.php" method="post">
  <input type="hidden" name="targetEmp" value="<?php echo $target; ?>">
  <table>
    <tr>
      <th></th>
      <th>Current</th>
      <th>New Value</th>
    </tr>
    <tr>
      <td>SSN</td>
      <td><?php echo $row['ssn'];?></td>
      <td>Immutable</td>
    </tr>
    <tr>
      <td>DOB</td>
      <td><?php echo date("m/d/Y", strtotime($row['dob']));?></td>
      <td><input name="newdob" type="date" max="<?php echo $maxDate; ?>"></td>
    </tr>
    <tr>
      <td>Hired</td>
      <td><?php echo date("m/d/Y", strtotime($row['hiredate']));?></td>
      <td><input name="newhired" type="date"></td>
    </tr>
    <tr>
      <td>Salary</td>
      <td><?php echo $row['salary'];?></td>
      <td><input name="newsal" type="number" min="0" max="1000000"></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td><input type="submit" value="Update"></td>
  </table>
</form>

<h2>Address</h2>
<form action="empdetails.php" method="post">
  <input type="hidden" name="targetEmp" value="<?php echo $target; ?>">
  <table>
    <tr>
      <th></th>
      <th>Current</th>
      <th>New Value</th>
    </tr>
    <tr>
      <td>Street</td>
      <td><?php echo $row['street'];?></td>
      <td><input type="text" name="newstreet"></td>
    </tr>
    <tr>
      <td>City</td>
      <td><?php echo $row['city'];?></td>
      <td><input type="text" name="newcity"></td>
    </tr>
    <tr>
      <td>State</td>
      <td><?php echo $row['state'];?></td>
      <td><input type="text" name="newstate" pattern="[A-Z]{2}" placeholder="AB"></td>
    </tr>
    <tr>
      <td>Zip</td>
      <td><?php echo $row['zip'];?></td>
      <td><input type="text" name="newzip" pattern="[0-9]{5}(-[0-9]{4})?" placeholder="12345[-6789]"></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td><input type="submit" value="Update"></td>
  </table>
</form>

<?php

  $result->free();
  $query = "SELECT did, name FROM Dept NATURAL JOIN WorksIn WHERE ssn = \"$target\"";
  $result = $db->query($query);
  $num_results = $result->num_rows;

  printf("<h2>Roles</h2>");
  printf("Works In: ");
  for($i = 0; $i < $num_results; $i++) {
    $row = $result->fetch_assoc();
    printf("%s", $row['name']);
    if($i != $num_results - 1) printf(", ");
  }
  printf("<br/>");
?>

<form action="empdetails.php" method="post">
Drop Role:
<input type="hidden" name="targetEmp" value="<?php echo $target; ?>">
<select name="dropdept">
  <?php
    $result->data_seek(0);
    for($i = 0; $i < $num_results; $i++) {
      $row = $result->fetch_assoc();
      printf("<option value=%s>%s</option>", $row['did'], $row['name']);
    }
  ?>
</select>
<input class="buttons" type="submit">
</form>

<form action="empdetails.php" method="post">
Add Role:
<input type="hidden" name="targetEmp" value="<?php echo $target; ?>">
<select name="newdept">
  <?php
    $result->free();
    $query = "SELECT did, name FROM Dept WHERE did NOT IN (SELECT did FROM WorksIn WHERE ssn=\"$target\")";
    $result = $db->query($query);
    $num_results = $result->num_rows;
    for($i = 0; $i < $num_results; $i++) {
      $row = $result->fetch_assoc();
      printf("<option value=%s>%s</option>", $row['did'], $row['name']);
    }
  ?>
</select>
<input class="buttons" type="submit">
</form>

<br />

<?php
  $result->free();
  $query = "SELECT name FROM Dept WHERE manager = \"$target\"";
  $result = $db->query($query);
  $num_results = $result->num_rows;

  printf("Manages: ");
  if(!$num_results) {
    printf("NONE");
  } else {
    for($i = 0; $i < $num_results; $i++) {
      $row = $result->fetch_assoc();
      printf("%s", $row['name']);
      if($i != $num_results - 1) printf(", ");
    }
  }

  $result->free();
  $db->close();
?>

<h2>Terminate</h2>
<form action="index.php" method="post">
  <input type="hidden" name="firee" value="<?php echo $target?>">
  <input class="buttons" type="submit" value="Go!">
</form>

</body>

</html>
