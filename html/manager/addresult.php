<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>Add Employee</title>
<link rel="stylesheet" type="text/css" href="hotelier.css">
</head>

<body>

<?php
  error_reporting(-1);
  ini_set('display_errors', 'On');
  include 'header.php';
  require_once 'credentials.php';
  $maxDate = date("Y-m-d", mktime(0, 0, 0, date("n"), date("j"), date("Y") - 18));
  echo 'PHP Version'.phpversion();

  @ $db = new mysqli('localhost', $mgruser, $mgrpass, 'hotelDB');
  if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit;
  }

  if (!empty($_POST['dob'])) {
    $dob = date("Y-m-d", strtotime($_POST['dob']));    
  }
  if (!empty($_POST['hire'])) {
    $hire = date("Y-m-d", strtotime($_POST['hire']));
  }

  $ssn = $db->real_escape_string($_POST['ssn']);
  $name = $db->real_escape_string($_POST['ename']);
  $salary = $db->real_escape_string($_POST['salary']);
  $street = $db->real_escape_string($_POST['street']);
  $city = $db->real_escape_string($_POST['city']);
  $state = $db->real_escape_string($_POST['state']);
  $zip = $db->real_escape_string($_POST['zip']);
  $depts = $_POST['depts'];

  $sql = "INSERT INTO Location (street, city, state, zip) VALUES (";  
  $sql .= empty($street) ? "NULL, " : "\"$street\", ";
  $sql .= empty($city) ? "NULL, " : "\"$city\", ";
  $sql .= empty($state) ? "NULL, " : "\"$state\", ";
  $sql .= empty($zip) ? "NULL)" : "\"$zip\")";
  echo "<p>1st insert: $sql</p>";

  if ($db->query($sql)) {
    echo "<p>Insert into Location table OK.</p>";
  } else {
    echo "<p>Insert into Location failed: ".$db->error."</p>";
    $insertFail = true;
    goto skipInserts;
  }

  $insert_id = $db->insert_id;
  $sql =  "INSERT INTO Emps VALUES (";
  $sql =. empty($ssn)     ? "NULL, " : "\"$ssn\", ";
  $sql =. empty($dob)     ? "NULL, " : "\"$dob\", ";
  $sql =. empty($name)    ? "NULL, " : "\"$name\", ";
  $sql =. empty($hire)    ? "NULL, " : "\"$hire\", ";
  $sql =. empty($salary)  ? "NULL, " : "\"$salary\", ";
  $sql =. "$insert_id)";
  echo "<p>2nd insert: $sql</p>";

  if ($db->query($sql)) {
    echo "<p>Insert into Emps table OK.</p>";
  } else {
    echo "<p>Insert into Emps failed: ".$db->error."</p>";
    $insertFail = true;
    goto skipInserts;
  }

  if(!empty($depts)) {
    echo 'Departments: ';
    for($i = 0; $i < count($depts); $i++) {
      echo $depts[$i]." ";
      $sql = "INSERT INTO WorksIn VALUES (\"$ssn\", ".$depts[$i].")";
    }
  }

  skipInserts:
  echo '<br />';
  if ($insertFail) {
    include 'addfail.php';
  } else {
    include 'addsuccess.php';
  }
  $db->close();
?>

</body>
</html>