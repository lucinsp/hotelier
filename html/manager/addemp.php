<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>Add Employee</title>
<link rel="stylesheet" type="text/css" href="hotelier.css">
</head>

<body>

<?php
  include 'header.php';
  require_once 'credentials.php';
  $maxDate = date("Y-m-d", mktime(0, 0, 0, date("n"), date("j"), date("Y") - 18));

  @ $db = new mysqli('localhost', $mgruser, $mgrpass, 'hotelDB');
  if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit;
  }  
?>

<h1>Hire New Employee</h1>

<form action="addresult.php" method="post">
<table>
  <tr>
    <th>Field</th>
    <th>Value</th>
  </tr>
  <tr>
    <td>SSN</td>
    <td><input name="ssn" type="text" size="11"
        pattern="[0-9]{3}-[0-9]{2}-[0-9]{4}" placeholder="123-45-6789" required></td>
  </tr>
  <tr>
    <td>DOB</td>
    <td>
    <?php
      printf("<input name=\"dob\" type=\"date\" max=\"%s\" required>", $maxDate);
    ?>
    </td>
  </tr>
  <tr>
    <td>Name</td>
    <td><input name="ename" type="text" required></td>
  </tr>
  <tr>
    <td>Hired</td>
    <td>
    <?php 
      printf("<input name=\"hire\" type=\"date\" max=\"%s\" required>", date("Y-m-d"));
    ?>
    </td> 
  </tr>
  <tr>
    <td>Salary</td>
    <td><input name="salary" type="number" min="0" max="999999" required></td>
  </tr>
  <tr>
    <td colspan="3"></td>
  </tr>
  <tr>
    <td>Street</td>
    <td><input name="street" type="text" required></td>
  </tr>
  <tr>
    <td>City</td>
    <td><input name="city" type="text" required></td>
  </tr>
  <tr>
    <td>State</td>
    <td><input name="state" type="text" size="2"
          pattern="[A-Z]{2}" placeholder="AB" required></td>
  </tr>
  <tr>
    <td>Zip</td>
    <td><input name="zip" type="text" pattern="[0-9]{5}(-[0-9]{4})?"
          placeholder="12345[-6789]" required></td>
  </tr>
</table>

<br />
Which departments will this person work in?<br/>
<?php
  $query = "SELECT did, name FROM Dept";
  $result = $db->query($query);
  $num_results = $result->num_rows;

  for($i = 0; $i < $num_results; $i++) {
    $row = $result->fetch_assoc();
    printf("<input type=\"checkbox\" name=\"depts[]\" value=\"%s\" />%s<br />", $row['did'], $row['name']);
  }

  $result->free();
  $db->close();
?>
<!-- <input type="checkbox" name="depts[]" value="A" />Acorn Building<br /> -->
<br />
<input type="submit"/>
</form>

</body>
</html>